package br.com.releasesolutions;

// Figura 12.7: LabelTest.java
// Testando LabelFrame

import javax.swing.*;

public class LabelTest {

    public static void main(String[] args) {

        LabelFrame labelFrame = new LabelFrame();
        labelFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        labelFrame.setSize(260, 180);
        labelFrame.setVisible(true);
    }

} // fim da classe LabelTest
